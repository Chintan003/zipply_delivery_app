Zipply is mainly focused to support building the delivery business. Zipply can be used for Grocery, Food Delivery, Fruits & Vegetable and Parcel/Courier delivery service .This is an app inspired by the Flutter framework, made by Google.

**Getting Started**

1. **System Requirements**

Based on Flutter requirement. Please select the operating system on which you are installing Flutter

    - Mac OS: https://flutter.dev/docs/get-started/install/macos
    - Window: https://flutter.dev/docs/get-started/install/windows
    - Linux: https://flutter.dev/docs/get-started/install/linux
    - Setup an editor: https://flutter.dev/docs/get-started/editor


2. **Setup Editor**

There are different editor you can use for running a flutter app but the most commonly used are Android Studio & VS Code. Here is a link to the official documentation from the flutter theme on how to setup these editors: https://flutter.dev/docs/get-started/editor?tab=androidstudio


3. **Installation**

After download and unzip the project, use the preferred IDE (Android Studio / Visual Code / IntelliJ) to open the project folder.

- Click the “Get dependencies” or “Packages get” to install the libraries from pubspecs.yaml file or run the following commad in the project folder.
    - **flutter packages get**
- Open the simulator to run iOS or Android (as the step above)
- Then press the run button to start the project.




