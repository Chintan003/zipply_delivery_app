import 'package:flutter/material.dart';
import 'package:zipply_app1/route.dart';

import 'config/theme.dart';
import 'ui/start/splash.dart';

AppThemeData theme = AppThemeData();

AppFoodRoute route = AppFoodRoute();

void main() {
  theme.init();
  runApp(AppZipply());
}

class AppZipply extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _theme = ThemeData(
      fontFamily: 'Roboto',
      primarySwatch: theme.primarySwatch,
    );

    if (theme.darkMode) {
      _theme = ThemeData(
        fontFamily: 'Roboto',
        brightness: Brightness.dark,
        unselectedWidgetColor: Colors.white,
        primarySwatch: theme.primarySwatch,
      );
    }

    return MaterialApp(
      title: 'Zipply', // "Food Delivery Flutter App UI Kit",
      debugShowCheckedModeBanner: false,
      theme: _theme,
      initialRoute: '/splash',
      //initialRoute: '/login',
      routes: {
        '/splash': (BuildContext context) => SplashScreen(),
      },
    );
  }
}
