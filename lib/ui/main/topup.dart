import 'package:flutter/material.dart';
import 'package:zipply_app1/widgets/creditCard.dart';
import 'package:zipply_app1/widgets/transferBank.dart';

class TopUp extends StatefulWidget {
  const TopUp({Key key}) : super(key: key);

  @override
  _TopUpState createState() => _TopUpState();
}

class _TopUpState extends State<TopUp> {
  double amount = 0.0;

  buttonPressed(String buttonText) {
    setState(() {
      amount = double.parse(buttonText);
    });
  }

  Widget buildButton(String buttonText) {
    return new Expanded(
        child: Container(
      margin: EdgeInsets.only(left: 15, right: 15, bottom: 5, top: 8),
      child: new FloatingActionButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          // side: BorderSide(color: Colors.white, width: 2.0)
        ),
        child: new Text(
          buttonText,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.bold, color: Colors.white),
        ),
        onPressed: () => buttonPressed(buttonText),
        backgroundColor: Color(0xFF950E0E),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Top Up",
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                  width: 400,
                  height: 150,
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Nominal: ',
                        style: TextStyle(fontSize: 15),
                      ),
                      Text(
                        '\$ ' + '$amount',
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      new Row(children: [
                        buildButton("10"),
                        buildButton("20"),
                        buildButton("50"),
                        buildButton("100")
                      ]),
                    ],
                  )),
            ),
            Container(
              color: Colors.white,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text('Payment Method', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold,),),
                ),
                ListTile(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => CreditCard()));
                  },
                  leading: CircleAvatar(
                    radius: 25.0,
                    child: ClipRRect(
                      // backgroundColor: Colors.white,
                      child: Image.asset(
                        'assets/imgs/visa_card.png',
                        fit: BoxFit.contain,
                      ),
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  title: Text("Credit Card",
                      style:
                          TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
                  subtitle: Text(
                      'Topup with Master Card/Visa/JCB/DISCOVER/Dinner Club or American Express',
                      style: TextStyle(fontSize: 13, color: Colors.black)),
                ),
                Divider(
                  height: 0.6,
                  color: Colors.grey,
                ),
                ListTile(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => TransferBank()));
                  },
                  leading: CircleAvatar(
                    radius: 25.0,
                    child: ClipRRect(
                      // backgroundColor: Colors.white,
                      child: Image.asset(
                        'assets/imgs/transfer_bank.png',
                        fit: BoxFit.contain,
                      ),
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  title: Text("Transfer Bank",
                      style:
                          TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
                  subtitle: Text('Topup with Manual Bank Transfer',
                      style: TextStyle(fontSize: 12, color: Colors.black)),
                ),
                Divider(
                  height: 0.6,
                  color: Colors.grey,
                ),
                ListTile(
                  leading: CircleAvatar(
                    radius: 25.0,
                    child: ClipRRect(
                      // backgroundColor: Colors.white,
                      child: Image.asset(
                        'assets/imgs/paypal.png',
                        fit: BoxFit.contain,
                      ),
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  title: Text("PAYPAL",
                      style:
                          TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
                  subtitle:
                      Text('Topup with Paypal', style: TextStyle(fontSize: 12, color: Colors.black)),
                ),
                Divider(
                  height: 0.6,
                  color: Colors.grey,
                ),
                ListTile(
                  leading: CircleAvatar(
                    radius: 25.0,
                    child: ClipRRect(
                      // backgroundColor: Colors.white,
                      child: Image.asset(
                        'assets/imgs/payumoney.png',
                        fit: BoxFit.contain,
                      ),
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  title: Text("PAYUMONEY",
                      style:
                          TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
                  subtitle: Text('Topup with PayuMoney',
                      style: TextStyle(fontSize: 12, color: Colors.black)),
                ),
              ]),
            )
          ],
        ),
      ),
    );
  }
}
