import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Favourite extends StatefulWidget {
  const Favourite({Key key}) : super(key: key);

  @override
  _FavouriteState createState() => _FavouriteState();
}

class _FavouriteState extends State<Favourite> {
  String text = "Favourite";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFF1F1F1),
        leading: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: CircleAvatar(
            backgroundColor: Color(0xFFF1F1F1),
            child: ClipRRect(
              child: SvgPicture.asset(
                'assets/imgs/app_logo1.svg',
              ),
              borderRadius: BorderRadius.circular(50.0),
            ),
          ),
        ),
      ),
    );
  }
}
