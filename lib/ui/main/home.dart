import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:zipply_app1/ui/main/topup.dart';

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String text = "Zipply";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFF1F1F1),
        leading: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: CircleAvatar(
            backgroundColor: Color(0xFFF1F1F1),
            child: ClipRRect(
              child: SvgPicture.asset(
                'assets/imgs/app_logo1.svg',
              ),
              borderRadius: BorderRadius.circular(50.0),
            ),
          ),
        ),
      ),
      body: Container(
        color: const Color(0xFFF1F1F1),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
          Container(
            margin: const EdgeInsets.all(10.0),
            width: 480.0,
            height: 120.0,
            decoration: BoxDecoration(
              color: const Color(0xFFA30202),
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
            child: Column(
              children: [
                Container(
                  decoration: new BoxDecoration(
                    color: Color(0xFF790505),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Wallet',
                            style:
                                TextStyle(fontSize: 15, color: Colors.white)),
                        SizedBox(
                          width: 150.0,
                        ),
                        Text('\$10000.0',
                            style:
                                TextStyle(fontSize: 15, color: Colors.white)),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      MaterialButton(
                        minWidth: 75,
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => TopUp()));
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.add_circle_rounded,
                              color: const Color(0xFFffffff),
                              size: 25,
                            ),
                            Text(
                              "Topup",
                              style:
                                  TextStyle(fontSize: 12, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                      MaterialButton(
                        minWidth: 75,
                        onPressed: () {},
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.arrow_circle_up,
                              size: 25,
                              color: const Color(0xFFffffff),
                            ),
                            Text(
                              "Withdraw",
                              style:
                                  TextStyle(fontSize: 12, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                      MaterialButton(
                        minWidth: 75,
                        onPressed: () {},
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.local_police_rounded,
                              size: 25,
                              color: const Color(0xFFffffff),
                            ),
                            Text(
                              "Promo",
                              style:
                                  TextStyle(fontSize: 12, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                      MaterialButton(
                        minWidth: 75,
                        onPressed: () {},
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.info_outline,
                              size: 25,
                              color: const Color(0xFFffffff),
                            ),
                            Text(
                              "Detail",
                              style:
                                  TextStyle(fontSize: 12, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
              child: GridView.count(
            crossAxisCount: 4,
            childAspectRatio: 0.95,
            children: [
              serviceWidget("scooter", "Bike"),
              serviceWidget("saloon_car", "Saloon Car"),
              serviceWidget("box", "Send Goods"),
              serviceWidget("truck", "Truck"),
              serviceWidget("car_rent", "Car Rent"),
              serviceWidget("food", "Food"),
              serviceWidget("shop", "Shop"),
              Column(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () => _show(context),
                      child: Container(
                        margin: EdgeInsets.only(
                            left: 15, right: 15, bottom: 5, top: 8),
                        decoration: BoxDecoration(
                          color: Color(0xFF8E8989),
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                        child: Center(
                          child: Container(
                            margin: EdgeInsets.all(15),
                            child: Stack(
                              children: [
                                SvgPicture.asset('assets/imgs/more.svg')
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'More',
                    style: TextStyle(fontSize: 11.0),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ],
          )),
          SizedBox(
            height: 1,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15.0, bottom: 5),
                child: Text('What Customer Says?', style: TextStyle(fontWeight: FontWeight.bold),),
              ),
              CarouselSlider(
                items: [
                  Container(
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            CircleAvatar(
                              backgroundImage: AssetImage('assets/imgs/profile_icon.png'),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0, left: 10, right: 10, bottom: 2),
                              child: Column(
                                children: [
                                  Text('Angad', style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),),
                                  Text('20 Jun 2021', style: TextStyle(fontSize: 12, color: Colors.grey),),
                                  Text('HSJ', style: TextStyle(fontSize: 12),)
                                ],
                              ),
                            ),
                            Icon(Icons.star, color: Colors.yellow[700], size: 20,),
                            Icon(Icons.star, color: Colors.yellow[700], size: 20,),
                            Icon(Icons.star, color: Colors.yellow[700], size: 20,),
                            Icon(Icons.star, color: Colors.yellow[700], size: 20,),
                            Icon(Icons.star, color: Colors.yellow[700], size: 20,),
                          ],
                        ),
                      )),
                  Container(
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            CircleAvatar(
                              backgroundImage: AssetImage('assets/imgs/profile_icon.png'),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0, left: 10, right: 10, bottom: 2),
                              child: Column(
                                children: [
                                  Text('Angad', style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),),
                                  Text('20 Jun 2021', style: TextStyle(fontSize: 12, color: Colors.grey),),
                                  Text('HSJ', style: TextStyle(fontSize: 12),)
                                ],
                              ),
                            ),
                            Icon(Icons.star, color: Colors.yellow[700], size: 20,),
                            Icon(Icons.star, color: Colors.yellow[700], size: 20,),
                            Icon(Icons.star, color: Colors.yellow[700], size: 20,),
                            Icon(Icons.star, color: Colors.yellow[700], size: 20,),
                            Icon(Icons.star, color: Colors.grey, size: 20,),
                          ],
                        ),
                      )),
                  Container(
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            CircleAvatar(
                              backgroundImage: AssetImage('assets/imgs/profile_icon.png'),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0, left: 10, right: 10, bottom: 2),
                              child: Column(
                                children: [
                                  Text('Angad', style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),),
                                  Text('20 Jun 2021', style: TextStyle(fontSize: 12, color: Colors.grey),),
                                  Text('HSJ', style: TextStyle(fontSize: 12),)
                                ],
                              ),
                            ),
                            Icon(Icons.star, color: Colors.yellow[700], size: 20,),
                            Icon(Icons.star, color: Colors.yellow[700], size: 20,),
                            Icon(Icons.star, color: Colors.yellow[700], size: 20,),
                            Icon(Icons.star, color: Colors.grey, size: 20,),
                            Icon(Icons.star, color: Colors.grey, size: 20,),
                          ],
                        ),
                      )),
                ],
                options: CarouselOptions(
                    height: 70.0,
                    autoPlay: true,
                    autoPlayCurve: Curves.bounceInOut,
                    enlargeCenterPage: true,
                    scrollDirection: Axis.horizontal),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text('Latest News', style: TextStyle(fontWeight: FontWeight.bold),),
                      SizedBox(width: 100,),
                      Text('Show All', style: TextStyle(color: Color(0xFFA30202),),)
                    ],
                  ),

                  Card(
                    color: Colors.white,
                    elevation: 4,
                    shape: RoundedRectangleBorder(
                      borderRadius: const BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    child: Container(
                      width: 200,
                      height: 130,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Icon(
                                  Icons.favorite,
                                  color: Colors.grey,
                                ),
                                SizedBox(
                                  width: 60,
                                ),
                                Text('Pizza & Food', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),),
                              ],
                            ),

                            SizedBox(
                              height: 55,
                            ),
                            Text('Nj Pizza New in Town', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
                            Text('Nj Pizza New in Town', style: TextStyle(fontSize: 12,),)
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Column serviceWidget(String img, String name) {
    return Column(
      children: [
        Expanded(
          child: InkWell(
            onTap: () {},
            child: Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 5, top: 8),
              decoration: BoxDecoration(
                color: Color(0xFF920707),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Center(
                child: Container(
                  margin: EdgeInsets.all(15),
                  child: Stack(
                    children: [SvgPicture.asset('assets/imgs/$img.svg')],
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          name,
          style: TextStyle(fontSize: 11.0),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  Column serviceMoreWidget(String img, String name) {
    return Column(
      children: [
        Expanded(
          child: InkWell(
            onTap: () {},
            child: Container(
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10, top: 8),
              decoration: BoxDecoration(
                color: Color(0xFF920707),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Center(
                child: Container(
                  margin: EdgeInsets.all(15),
                  child: Stack(
                    children: [SvgPicture.asset('assets/imgs/$img.svg')],
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 5.0, right: 5, bottom: 5),
          child: Text(
            name,
            style: TextStyle(fontSize: 11.0),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(
          height: 10,
        )
      ],
    );
  }

  void _show(BuildContext ctx) {
    showModalBottomSheet(
      isScrollControlled: true,
      elevation: 5,
      context: ctx,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      backgroundColor: Color(0xFFF1F1F1),
      builder: (ctx) => Padding(
        padding: EdgeInsets.only(top: 10),
        child: Expanded(
          child: GridView.count(
            shrinkWrap: true,
            crossAxisCount: 4,
            childAspectRatio: 0.85,
            children: [
              serviceMoreWidget("scooter", "Bike"),
              serviceMoreWidget("saloon_car", "Saloon Car"),
              serviceMoreWidget("box", "Send Goods"),
              serviceMoreWidget("truck", "Truck"),
              serviceMoreWidget("car_rent", "Car Rent"),
              serviceMoreWidget("food", "Food"),
              serviceMoreWidget("shop", "Shop"),
              serviceMoreWidget("groceries", "Grocery"),
              serviceMoreWidget("medicine_pills", "Medicine"),
              serviceMoreWidget("suv_car", "SUV Car"),
              serviceMoreWidget("box", "Van Shipment"),
              serviceMoreWidget("suv_car", "Hatchback Car"),
              serviceMoreWidget("car_rent", "SUV Rent Car"),
              serviceMoreWidget("tuktuk", "Tuktuk"),
            ],
          ),
        ),
      ),
    );
  }
}
