import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Profile extends StatefulWidget {
  const Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String text = "Profile";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFF1F1F1),
        leading: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: CircleAvatar(
            backgroundColor: Color(0xFFF1F1F1),
            child: ClipRRect(
              child: SvgPicture.asset(
                'assets/imgs/app_logo1.svg',
              ),
              borderRadius: BorderRadius.circular(50.0),
            ),
          ),
        ),
      ),
      body: SafeArea(
          child: Column(
        children: [
          //for circle avtar image
          _getHeader(),
          SizedBox(
            height: 10,
          ),
          SizedBox(
            height: 14,
          ),
          _heading("Personal Details"),
          SizedBox(
            height: 6,
          ),
          _detailsCard(),
          SizedBox(
            height: 5,
          ),
          _heading("About Us"),
          SizedBox(
            height: 6,
          ),
          _settingsCard(),
          Spacer(),
          logoutButton()
        ],
      )),
    );
  }

  Widget _getHeader() {
    return Column(
      children: [
        ListTile(
          leading: CircleAvatar(
            radius: 30,
            // backgroundColor: Colors.white,
            backgroundImage: AssetImage('assets/imgs/profile_icon.png'),
          ),
          title: Text("Chintan Devani",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
          subtitle: Text('chintandevani003@gmail.com',
              style: TextStyle(fontSize: 13)),
        ),
      ],
    );
  }

  Widget _heading(String heading) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.80, //80% of width,
      child: Text(
        heading,
        style: TextStyle(fontSize: 16),
      ),
    );
  }

  Widget _detailsCard() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            //row for each deatails
            ListTile(
              leading: Icon(Icons.edit),
              title: Text("Edit Profile"),
            ),
            Divider(
              height: 0.6,
              color: Colors.black87,
            ),
            ListTile(
              leading: Icon(Icons.lock_rounded),
              title: Text("Change Password"),
            ),
          ],
        ),
      ),
    );
  }

  Widget _settingsCard() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            //row for each deatails
            ListTile(
              leading: Icon(Icons.privacy_tip),
              title: Text("Privacy"),
            ),
            Divider(
              height: 0.6,
              color: Colors.black87,
            ),
            ListTile(
              leading: Icon(Icons.share_rounded),
              title: Text("Share App"),
            ),
            Divider(
              height: 0.6,
              color: Colors.black87,
            ),
            ListTile(
              leading: Icon(Icons.dashboard_customize),
              title: Text("About Us"),
            ),
            Divider(
              height: 0.6,
              color: Colors.black87,
            ),
            ListTile(
              leading: Icon(Icons.star_rate_sharp),
              title: Text("Rate App"),
            )
          ],
        ),
      ),
    );
  }

  Widget logoutButton() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          children: [
            //row for each deatails
            InkWell(
              onTap: () {},
              child: Container(
                  // color: Colors.orange,
                  child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.logout,
                    ),
                    SizedBox(width: 10),
                    Text(
                      "Logout",
                      style: TextStyle( fontSize: 18),
                    )
                  ],
                ),
              )),
            ),
          ],
        ),
      ),
    );
  }
}
