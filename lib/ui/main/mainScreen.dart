import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zipply_app1/ui/main/pr.dart';

import 'chat.dart';
import 'home.dart';
import 'favourite.dart';
import 'order.dart';
import 'profile.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  int currentTab = 0;
  final List<Widget> screens = [
    Home(),
    Order(),
    Favourite(),
    Chat(),
    Profile()

  ];

  Widget currentScreen = Home();

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),

      bottomNavigationBar: BottomAppBar(

        // shape: CircularProgressIndicator,
        child: Container(
          color: Colors.white,
          height: 60,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 0.0),
            child: Row(
              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      MaterialButton(
                        minWidth: 60,
                        onPressed: (){
                          setState(() {
                            currentScreen = Home();
                            currentTab = 0;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.home,
                              color: currentTab == 0? Color(0xFFA30202): Colors.grey, ),
                            Text("Home",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: currentTab == 0? Color(0xFFA30202): Colors.grey,),),
                          ],
                        ),
                      ),
                      MaterialButton(
                        minWidth: 60,
                        onPressed: (){
                          setState(() {
                            currentScreen = Order();
                            currentTab = 1;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.reorder,
                              color: currentTab == 1? Color(0xFFA30202): Colors.grey,),
                            Text("Order",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: currentTab == 1? Color(0xFFA30202): Colors.grey,),),
                          ],
                        ),
                      ),
                      MaterialButton(
                        minWidth: 60,
                        onPressed: (){
                          setState(() {
                            currentScreen = Favourite();
                            currentTab = 2;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.favorite,
                              color: currentTab == 2? Color(0xFFA30202): Colors.grey,),
                            Text("Favourite",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: currentTab == 2? Color(0xFFA30202): Colors.grey,),),
                          ],
                        ),
                      ),
                      MaterialButton(
                        minWidth: 60,
                        onPressed: (){
                          setState(() {
                            currentScreen = Chat();
                            currentTab = 3;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.chat,
                              color: currentTab == 3? Color(0xFFA30202): Colors.grey,),
                            Text("Chat",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: currentTab == 3? Color(0xFFA30202): Colors.grey,),),
                          ],
                        ),
                      ),
                      MaterialButton(
                        minWidth: 60,
                        onPressed: (){
                          setState(() {
                            currentScreen = Profile();
                            currentTab = 4;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.account_circle,
                              color: currentTab == 4? Color(0xFFA30202): Colors.grey,),
                            Text("Profile",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: currentTab == 4? Color(0xFFA30202): Colors.grey,),),
                          ],
                        ),
                      ),

                    ]
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}