import 'dart:async';
import 'package:flutter/material.dart';
import '../../main.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  _startNextScreen() {
    route.setDuration(1);
    route.pushToStart(context, "/onboard");
  }

  var windowWidth;
  var windowHeight;

  @override
  void initState() {
    super.initState();
    startTime();
  }

  startTime() async {
    var duration = new Duration(seconds: 3);
    return Timer(duration, _startNextScreen);
  }

  @override
  Widget build(BuildContext context) {
    windowWidth = MediaQuery.of(context).size.width;
    windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: initScreen(context),
    );
  }

  initScreen(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          color: theme.colorBackground,
        ),

        // IBackground4(width: windowWidth, colorsGradient: theme.colorsGradient),

        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Hero(
                tag: "SplashLogo",
                child: Container(
                  width: windowWidth * 0.7,
                  child: Image.asset("assets/imgs/logo1.png", fit: BoxFit.cover),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 20.0)),
              // CircularProgressIndicator(
              //   backgroundColor: theme.colorCompanion4,
              //   strokeWidth: 1,
              // )
            ],
          ),
        ),
      ],
    ));
  }
}
