import 'package:flutter/material.dart';

class Slide {
  final String imageUrl1;
  final String imageUrl2;
  final String imageUrl3;
  final String title;
  final String description;
  final String subtitle;

  Slide({
    @required this.imageUrl1,
    @required this.imageUrl2,
    @required this.imageUrl3,
    @required this.title,
    @required this.subtitle,
    @required this.description,
  });
}

final slideList = [
  Slide(
    imageUrl1: 'assets/imgs/certification.png',
    imageUrl2: 'assets/imgs/box.png',
    imageUrl3: 'assets/imgs/give.png',
    title: 'Shipping Solution\nFor All',
    subtitle: 'Pickup and Delivery Anywhere',
    description: 'Food drinks, groceries, and more available for delivery and pickup.',
  ),
  Slide(
    imageUrl1: 'assets/imgs/gift.png',
    imageUrl2: 'assets/imgs/payment_bill.png',
    imageUrl3: 'assets/imgs/groceries.png',
    title: 'Save Big \nOn Every Order.',
    subtitle: 'Join Unlimited.',
    description: 'Unlimited free deliveries. Reduced fees on eligible order.',
  ),


];
