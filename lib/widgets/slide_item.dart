import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../model/slide.dart';

class SlideItem extends StatelessWidget {
  final int index;
  SlideItem(this.index);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: SvgPicture.asset(
                'assets/imgs/app_logo.svg',
                width: 50,
                height: 50,
              ),
            ),
            Column(
              children: [
                Text(
                  'ZIPPLY',
                  style: TextStyle(
                      fontSize: 30, color: Colors.white, fontFamily: 'Roboto'),
                ),
                Text(
                  'GET IT IN A ZIP',
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                )
              ],
            )
          ],
        ),
        SizedBox(
          height: 170,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(15.0, 15.0, 25.0, 0.0),
          child: Row(children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                width: 50,
                height: 50,
                margin: EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  image: DecorationImage(
                      image: AssetImage(slideList[index].imageUrl1),
                      fit: BoxFit.contain),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  image: DecorationImage(
                    image: AssetImage(slideList[index].imageUrl2),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  image: DecorationImage(
                    image: AssetImage(slideList[index].imageUrl3),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
          ]),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 0.0),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
              slideList[index].title,
              style: TextStyle(
                fontSize: 25,
                color: Colors.white,
                fontFamily: 'Roboto',
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0.0, 7.0, 0.0, 7.0),
              child: Text(
                slideList[index].subtitle,
                style: TextStyle(color: Colors.white, fontSize: 18),
                // textAlign: TextAlign.center,
              ),
            ),
            Text(
              slideList[index].description,
              style: TextStyle(color: Colors.white, fontSize: 10),
              // textAlign: TextAlign.center,
            ),
          ]),
        ),
      ],
    );
  }
}
